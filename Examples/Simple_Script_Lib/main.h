/**
 * @file main.h
 * The main header file of the library. Contains include statements for the
 * entire library.
 *
 * @author Sarmad Khalid Abdulla
 *
 * @date 2013-01-03 : Created.
 */
////////////////////////////////////////////////////////////////////////////////

#include <core.h>

using namespace Core;

// Simple_Script_Lib's header files
#include "Script_Parsing_Handler.h"
#include "Library_Gateway.h"
