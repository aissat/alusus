/**
 * @file Scg/Memory/AutoDeleteAllocator.cpp
 *
 * @copyright Copyright (C) 2014 Rafid Khalid Abdullah
 *
 * @license This file is released under Alusus Public License, Version 1.0.
 * For details on usage and copying conditions read the full license in the
 * accompanying license file or at <http://alusus.net/alusus_license_1_0>.
 */
//==============================================================================

#include <prerequisites.h>
#include <Memory/AutoDeleteAllocator.h>

namespace Scg
{
AutoDeleteAllocator AutoDeleteAllocator::s_singleton;
}
