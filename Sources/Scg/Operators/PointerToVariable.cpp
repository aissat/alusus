/**
 * @file Scg/Operators/PointerToVariable.cpp
 *
 * @copyright Copyright (C) 2014 Rafid Khalid Abdullah
 *
 * @license This file is released under Alusus Public License, Version 1.0.
 * For details on usage and copying conditions read the full license in the
 * accompanying license file or at <http://alusus.net/alusus_license_1_0>.
 */
//==============================================================================

#include <prerequisites.h>
// LLVM header files
#include <llvm/IR/IRBuilder.h>
// Scg files
#include <Containers/Block.h>
#include <Operators/PointerToVariable.h>
#include <Types/PointerType.h>

namespace Scg
{
PointerToVariable::~PointerToVariable()
{
  if (this->valueType != nullptr)
    delete this->valueType;
}

//------------------------------------------------------------------------------

const ValueTypeSpec *PointerToVariable::GetValueTypeSpec() const
{
  BLOCK_CHECK;

  if (this->valueType)
    return this->valueType->GetValueTypeSpec();

  auto var = GetBlock()->GetVariable(this->name);
  if (var == 0)
    throw EXCEPTION(UndefinedVariableException,
        ("Referencing undefined variable: " + this->name).c_str());

  this->valueType = new PointerType(*var->GetValueTypeSpec()->ToValueType(*GetModule()));
  return this->valueType->GetValueTypeSpec();
}

//------------------------------------------------------------------------------

Expression::CodeGenerationStage PointerToVariable::GenerateCode()
{
  BLOCK_CHECK;

  // Look up the variable.
  auto var = GetBlock()->GetVariable(this->name);
  if (!var)
    throw EXCEPTION(UndefinedVariableException, ("Undefined variable: " + this->name).c_str());

  // The LLVM Alloca instruction used to allocate the variable is a pointer to
  // the variable, so we just use it as the value generated by this expression.
  this->generatedLlvmValue = var->GetLlvmValue();

  return Expression::GenerateCode();
}

//----------------------------------------------------------------------------

std::string PointerToVariable::ToString()
{
  // TODO: Do we need to use std::stringstream?
  return this->name + "~ptr";
}
}
