/**
 * @file Tests/CoreTests/core_tests.h
 * Contains the CoreTests namespace main include statements.
 *
 * @copyright Copyright (C) 2014 Sarmad Khalid Abdullah
 *
 * @license This file is released under Alusus Public License, Version 1.0.
 * For details on usage and copying conditions read the full license in the
 * accompanying license file or at <http://alusus.net/alusus_license_1_0>.
 */
//==============================================================================

#ifndef CORE_TESTS_H
#define CORE_TESTS_H

#include "core.h"
#include <string.h>
#include <sstream>

#undef __WIN32__
#undef _WIN32
#undef _MSC_VER
#include <catch.hpp>

#include "helpers.h"
#include "TestGrammarPlant.h"
#include "TestEngine.h"

#endif // CORE_TESTS_H
